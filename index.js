import express from "express";
import dotenv from "dotenv";

dotenv.config();

const PORT = process.env.PORT || 3000;
const VERSION = process.env.VERSION || "0.0.0";

const app = express();

app.get("/", (_request, response) => {
    response.send("Muumipeikko ja tapiiri " + VERSION);
});

app.get("/version", (_request, response) => {
    response.send("Very new version 2.0.1");
});


app.listen(PORT, () => {
    console.log(`Express server listening on PORT ${PORT}`);
});