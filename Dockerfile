FROM node

WORKDIR /app

COPY ./package*.json .

RUN npm install

COPY ./index.js .

ARG APP_PORT=3000
ENV PORT=${APP_PORT}

ARG VERSION
ENV VERSION=${VERSION}

EXPOSE ${APP_PORT}

CMD ["npm", "start"]

